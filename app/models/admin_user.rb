class AdminUser < ApplicationRecord
    #because this class automaticattly calls users table which no longer exists.
    #it has been changed to admin_users
    #self.table_name="admin_users" _

    has_and_belongs_to_many :pages
    has_many :section_edits
    has_many :sections, :through => :section_edits


end
