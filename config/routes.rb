Rails.application.routes.draw do

  resources :subjects do
    member do
      get :delete
    end
  end

  resources :pages do
    member do
      get :delete
    end
  end

  resources :sections do
    member do
      get :delete
    end
  end


  # get 'subjects/index'
  # get 'subjects/show'
  # get 'subjects/new'
  # get 'subjects/edit'
  # get 'subjects/delete'

  #get 'test/display'
  #get 'demo2/index'
  #simple route (system automatically creates simple route of a controller)
  #get 'demo/index'
  #get 'demo/hello'
  # get 'demo/learning_render'
  # get 'demo/external_url'
  # root 'demo#index'

  #root route
 
  


  #default route
  get ':controller(/:action(/:id))' 
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
